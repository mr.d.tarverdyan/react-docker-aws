#!/bin/sh

echo "Pre-Build Steps:"
echo "authentication with AWS ECR..."
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 538634739657.dkr.ecr.us-east-1.amazonaws.com

echo "Build Steps:"
echo "Building image..."
#docker build -t 538634739657.dkr.ecr.us-east-1.amazonaws.com:latest .
docker build -t react-docker-aws .

echo "Tag Steps:"
echo "Taging image..."
docker tag react-docker-aws:latest 538634739657.dkr.ecr.us-east-1.amazonaws.com/react-docker-aws:latest

echo "Post-Build Steps:"
echo "Pushing image to AWS ECR..."
#docker push 538634739657.dkr.ecr.us-east-1.amazonaws.com:latest
docker push 538634739657.dkr.ecr.us-east-1.amazonaws.com/react-docker-aws:latest

echo "Updating AWS ECS service..."
aws ecs update-service --cluster react-cluster --service react-sv --force-new-deployment

echo "Done!"
